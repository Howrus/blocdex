import 'package:blocdex/ui/screens/pokedex/pokedex_screen.dart';
import 'package:flutter/material.dart';


class BlocDex extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BLoC Pokedex',
      theme: ThemeData(
        primarySwatch: Colors.red
      ),
      home: PokedexScreen()
    );
  }
}